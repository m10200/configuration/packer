#!/bin/bash

sleep 30
sudo aws configure set default.region eu-central-1
sudo amazon-linux-extras install docker
sudo systemctl start docker
sudo systemctl enable docker
sudo aws ecr get-login-password | sudo docker login --username AWS --password-stdin 432806165674.dkr.ecr.eu-central-1.amazonaws.com
sudo docker pull 432806165674.dkr.ecr.eu-central-1.amazonaws.com/conduit_front_end:latest
sudo rm /root/.docker/config.json
sudo docker run -p 80:80 --detach --restart=always 432806165674.dkr.ecr.eu-central-1.amazonaws.com/conduit_front_end 


